#!/usr/bin/bash

# Избавляемся от использования ковычек. Используем в командной строке
# ключ -v для передачи gawk номера столбца в виде переменной.

if [ $# != 2 ]
then
    echo "Порядок использования: msbl_12p573.sh номер_столбца имя_файл"
    exit 1
fi
gawk -v "field=$1" < $2 '
      {count[$field]++}
END {for (item in count) printf "%-20s%-20s\n",\
			     item, count[item]'} | 
    sort

