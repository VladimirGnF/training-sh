#!/usr/bin/bash

# Сценарий
# - содержит проверку на отсутствие ошибок вх.аргументов. 
# - ведет подсчет содержимого столбца в файле.

if [ $# != 2 ]
then
    echo "Порядок использования: msbl_12p572.sh поля файл"
    exit 1
fi
gawk < $2 '
      {count[$'$1']++}
END {for (item in count) printf "%-20s%-20s\n",\
			     item, count[item]'} | 
    sort

