
# Получение вывода из сети.

BEGIN{
    # Присвоение переменной 'server' специального сетевого имени файла.
    server = "/inet/tcp/0/www.rfc-editor.org/80"
    # Использование сопроцессора для отправки GET-запроса на удаленный
    # сервер. 
    print  "GET /rfc/rfc-retrieval.txt" |& server
    # Цикл while использует сопроцессор для перенаправления вывода с
    # сервера инструкции getline
    while (server |& getline)
	print $0
}
