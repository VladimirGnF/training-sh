#! /bin/bash

gawk < /etc/passwd ' BEGIN {
    uid[void] = ""                                                      # Сообщение для gawk, что uid - массив.
}
{                                                                       # Шаблон отсутствует, т.е. обрабатываем все записи.
    dup = 0                                                             # Инициализация флага дублирования.
    split( $0, field, ":")                                              # Разделение на поля, разделенные ':'.
    
    if (field[2] == "")                                                 # Проверка на нулевую строку поля пароля.
    {
	if (field[5] == "")                                             # Проверка на нулевую строку поля info.
	{
	    print field[1] " не имеет пароля."
	}
	else
	{
	    print field[1] " ("field[5]") не имеет пароля."
	}
    }
    for (name in uid)                                                   # Перебор элементов массива uid.
    {
	if (uid[name] == field[3])                                      # Проверка повторного использования UID.
	{
	    print field[1] " имеет тот же UID, что и " name " : UID = " uid[name]
	    dup = 1                                                     # Установка флага дублирования.
	}
    }
    if (!dup)                                                           # То же самое, что и if (dup == 0).
    {
	uid[field[1]] = field[3]                                        # Присваивание  UID и имени пользователя массиву.
    }
}'
