# Управляющая структура while

# gawk -f ./msbl_12p560_while.awk

BEGIN {
    n = 1
    while (n <= 5)
    {
	print "2^" n, 2**n
	n++
    }
}

