#! /bin/bash

trap 'rm -f $$.tem > /dev/null;echo $0 aborted.;exit 1' 1 2 15
echo -n "Диаппазон цен (пример: 5000 7500):"
read lowrange hirange
echo '
                                        Мили
Производитель   Модель          Год     (x0000) Цена
--------------------------------------------------' > $$.tem
gawk < icars.txt '
$5 >= '$lowrange' && $5 <= '$hirange' {
if ($1 ~ /ply/) $1 = "plymouth"
if ($1 ~ /chev/) $1 = "chevrolet"
printf "%-10s \t%-8s \t%2d %5d \t$ %8.2f\n", $1, $2, $3, $4, $5 }' | sort -n +5 >> $$.tem
cat $$.tem
rm $$.tem
