#!/usr/bin/bash

red='\e[1;31m%s\e[0m\n'
green='\e[1;32m%s\e[0m\n'
yellow='\e[1;33m%s\e[0m\n'
blue='\e[1;34m%s\e[0m\n'
magenta='\e[1;35m%s\e[0m\n'
cyan='\e[1;36m%s\e[0m\n'

# background color using ANSI escape

bgBlack=$(tput setab 0)   # black
bgRed=$(tput setab 1)     # red
bgGreen=$(tput setab 2)   # green
bgYellow=$(tput setab 3)  # yellow
bgBlue=$(tput setab 4)    # blue
bgMagenta=$(tput setab 5) # magenta
bgCyan=$(tput setab 6)    # cyan
bgWhite=$(tput setab 7)   # white

# foreground color using ANSI escape

fgBLack=$(tput setaf 0)   # black
fgRed=$(tput setaf 1)     # red
fgGreen=$(tput setaf 2)   # green
fgYellow=$(tput setaf 3)  # yellow
fgBlue=$(tput setaf 4)    # blue
fgMagenta=$(tput setaf 5) # magenta
fgCyan=$(tput setaf 6)    # cyan
fgWhite=$(tput setaf 7)   # white

# text editing options

txBold=$(tput bold)       # bold
txHalf=$(tput dim)        # half-bright
txUnderline=$(tput smul)  # underline
txEndUnder=$(tput rmul)   # exit underline
txReverse=$(tput rev)     # reverse
txStandout=$(tput smso)   # standout
txEndStand=$(tput rmso)   # exit standout
txReset=$(tput sgr0)      # reset attributes

echo "
${fgGreen}01) gawk '{ print }' icars.txt
${fgCyan}gawk выводит все строки, т.к. отсутствует шаблон.
В print отсутствуют аргументы, поэтому выводиться вся строка целиком.   
${txReset}"

gawk '{ print }' icars.txt 

echo "
${fgGreen}02) gawk '/chevy/' icars.txt
${fgCyan}gawk выбирает строки в которых содержиться строковое значение 
chevy. Когда действие не указано, то gawk предполагает что необходимо
использовать команду print.
${txReset}"

gawk '/chevy/' icars.txt 

echo "
${fgGreen}03) gawk '{print $3, $1}' icars.txt
${fgCyan}Действие заключено в {} для того чтобы утилита gawk смогла отличить
действие от шаблона.
${txReset}"

gawk '{print $3, $1}' icars.txt 

echo "
${fgGreen}04) gawk '/chevy/ {print $3, $1}' icars.txt
${fgCyan} Включены шаблон и действие.
${txReset}"

gawk '/chevy/ {print $3, $1}' icars.txt 

echo "
${fgGreen}05) gawk '/h/' icars.txt
${fgCyan} Выбираем строки, которые соответствуют регулярному выражению
'/h/'.
${txReset}"

gawk '/h/' icars.txt 

echo "
${fgGreen}06) gawk '$1 ~ /h/' icars.txt
${fgCyan} Выбираем строки, которые содержат в первом поле букву h,
где '~' - оператор соответствия.
${txReset}"

gawk '$1 ~ /h/' icars.txt 

echo "
${fgGreen}07) gawk -f ./msbl_12p565.awk icars.txt
${fgCyan}cat ./msbl_565.awk ${fgYellow}"
cat ./msbl_565.awk 
echo"${txReset}"

gawk -f ./msbl_565.awk icars.txt 

echo "
${fgGreen}08) gawk -f ./msbl_566.awk icars.txt
${fgCyan}cat ./msbl_566.awk ${fgYellow}" 
cat ./msbl_566.awk 
echo "${txReset}"

gawk -f ./msbl_566.awk icars.txt 

echo "
${fgGreen}09) gawk '{print length, $0}' icars.txt | sort -n
${fgCyan} lenght - длинна строки;
$0 - текущий номер строки;
sort -n - числовая сортировка.
${txReset}"

gawk '{print length, $0}' icars.txt | sort -n

echo "
${fgGreen}10) gawk 'length > 24 {print NR}' icars.txt
${fgCyan}lenght > 24 - выбираем строки с длинной строки более 24 символов;
NR - номер выбранной строки.
${txReset}"

gawk 'length > 24 {print NR}' icars.txt

echo "
${fgGreen}11) gawk 'NR == 2 , NR == 4' icars.txt
${fgCyan}',' - оператор диапазона
В данном примере выбираем со 2-ой по 4-ую строки.
${txReset}"

gawk 'NR == 2 , NR == 4' icars.txt

echo "
${fgGreen}11) gawk 'END {print NR, \"автомобилей на продажу.\"}' icars.txt
${fgCyan}'END' - gawk выполняет действие после обработки последней строки.
${txReset}"

gawk 'END {print NR, "автомобилей на продажу."}' icars.txt

echo "
${fgGreen}12) gawk -f ./msbl_567_if.awk icars.txt
${fgCyan}cat ./msbl_567_if.awk ${fgYellow}"
cat ./msbl_567_if.awk 
echo "${txReset}"

gawk -f ./msbl_567_if.awk icars.txt 

echo "
${fgGreen}13) ./msbl_568_if.awk icars.txt
${fgCyan}cat ./msbl_568_if.awk ${fgYellow}"
cat ./msbl_568_if.awk 
echo "${txReset}"

./msbl_568_if.awk icars.txt 

echo "
${fgGreen}14) gawk -f ./msbl_568_OFS.awk icars.txt
${fgCyan}cat ./msbl_568_OFS.awk ${fgYellow}"
cat ./msbl_568_OFS.awk 
echo "${txReset}"

gawk -f ./msbl_568_OFS.awk icars.txt 

echo "
${fgGreen}14) gawk -f ./msbl_569_printformat.awk icars.txt
${fgCyan}cat ./msbl_569_printformat.awk ${fgYellow}"
cat ./msbl_569_printformat.awk 
echo "${txReset}"

gawk -f ./msbl_569_printformat.awk icars.txt

echo "
${fgGreen}15) gawk -f ./msbl_569_out.awk icars.txt
${fgCyan}cat ./msbl_569_out.awk ${fgYellow}"
cat ./msbl_569_out.awk 
echo "${txReset}"

gawk -f ./msbl_569_out.awk icars.txt

echo "
${fgGreen}16) gawk -f ./msbl_570.awk icars.txt
${fgCyan}cat ./msbl_570.awk ${fgYellow}"
cat ./msbl_570.awk
echo "${txReset}"

gawk -f ./msbl_570.awk icars.txt

echo "
${fgGreen}17) gawk '/root/{print}' /etc/passwd
${fgCyan}Показывает формат строки из файла passwd.
${txReset}"

gawk '/root/{print}' /etc/passwd 

echo "
${fgGreen}16) gawk -f ./msbl_571.awk /etc/passwd
${fgCyan}cat ./msbl_571.awk ${fgYellow}"
cat ./msbl_571.awk
echo "${txReset}"

gawk -f ./msbl_571.awk /etc/passwd

echo "
${fgGreen}17) gawk '/root/{print}' /etc/passwd
${fgCyan}Показывает формат строки из файла passwd.
${txReset}"

gawk '/root/{print}' /etc/passwd 

echo "
${fgGreen}18) gawk -f ./msbl_571_ifelseif.awk icars.txt 
${fgCyan}cat ./msbl_571_ifelseif.awk ${fgYellow}"
cat ./msbl_571_ifelseif.awk
echo "${txReset}"

gawk -f ./msbl_571_ifelseif.awk ./icars.txt 

echo "
${fgGreen}19) ./msbl_571_for.sh ./icars.txt 
${fgCyan}cat ./msbl_571_for.sh ${fgYellow}"
cat ./msbl_571_for.sh 
echo "${txReset}"

./msbl_571_for.sh ./icars.txt 

echo "
${fgGreen}19) ./msbl_572.sh 
${fgCyan}cat ./msbl_572.sh ${fgYellow}"
cat ./msbl_572.sh 
echo "${txReset}"

./msbl_572.sh

echo "
${fgGreen}19(a)) ./msbl_572.sh 1 ./icars.txt 
${txReset}"
./msbl_572.sh 1 ./icars.txt

echo "
${fgGreen}19(b)) ./msbl_572.sh 3 ./icars.txt 
${txReset}"
./msbl_572.sh 3 ./icars.txt

echo "
${fgGreen}20) ./msbl_574_report.sh icars.txt 
${fgCyan}cat ./msbl_574_report.sh ${fgYellow}"
cat ./msbl_574_report.sh 
echo "${txReset}"

./msbl_574_report.sh ./icars.txt 

echo "
${fgGreen}21) ./msbl_575.sh
${fgCyan}cat ./msbl_575.sh ${fgYellow}"
cat ./msbl_575.sh
echo "${txReset}"

./msbl_575.sh 

echo "
${fgGreen}21) ./msbl_577.sh
${fgCyan}cat ./msbl_577.sh ${fgYellow}"
cat ./msbl_577.sh
echo "${txReset}"

./msbl_577.sh

echo "
${fgGreen}22) ./msbl_578_listcars.sh
${fgCyan}cat ./msbl_578.sh ${fgYellow}"
cat ./msbl_578_listcars.sh 
echo "${txReset}"

# ./msbl_578_listcars.sh

echo " ${fgYellow}
------------------------------------------------------------------------
                    Extend programming of GAWK.
------------------------------------------------------------------------
${txReset}"

echo "
${fgGreen}23) echo aa | gawk -f ./msbl_579_4gawk_getline.awk 
${fgCyan}cat ./msbl_579_4gawk_getline.awk ${fgYellow}"
cat ./msbl_579_4gawk_getline.awk 
echo "${txReset}"

echo aa | gawk -f ./msbl_579_4gawk_getline.awk

echo "
${fgGreen}24) gawk -f ./msbl_579_4gawk_getline.awk < ialpha.txt 
${fgCyan}cat Когда на вход программы подается несколько строк, то 
getline всё равно считывает только первую строку.
${txReset}"

gawk -f ./msbl_579_4gawk_getline.awk < ialpha.txt

echo "
${fgGreen}25) gawk 'BEGIN {getline; print $1}' < ialpha.txt
${fgCyan} Когда для getline отсутствуют аргументы, то инструкция считвает
ввод в переменную $0 и модифицирует переменные полей ($1, $2, ...).
${txReset}"

gawk 'BEGIN {getline; print $1}' < ialpha.txt


echo "
${fgGreen}24) gawk 'BEGIN {getline; print $1}' < ialpha.txt
${fgCyan} Когда для getline отсутствуют аргументы, то инструкция считвает
ввод в переменную $0 и модифицирует переменные полей ($1, $2, ...).
${txReset}"

gawk 'BEGIN {getline; print $1}' < ialpha.txt

echo "
${fgGreen}25) gawk -f ./msbl_579_4gawk_getline2.awk < ialpha.txt 
${fgCyan}cat ./msbl_579_4gawk_getline2.awk ${fgYellow}"
cat ./msbl_579_4gawk_getline2.awk 
echo "${txReset}"

gawk -f ./msbl_579_4gawk_getline2.awk < ialpha.txt 

echo "${txReset}"

echo "
${fgGreen}26) gawk -f ./msbl_580_4gawk_getline3.awk < ialpha.txt 
${fgCyan}cat ./msbl_580_4gawk_getline3.awk ${fgYellow}"
cat ./msbl_580_4gawk_getline3.awk 
echo "${txReset}"

gawk -f ./msbl_580_4gawk_getline3.awk < ialpha.txt 

echo "
${fgGreen}27) gawk -f ./msbl_580_4gawk_getline4.awk < ialpha.txt 
${fgCyan}cat ./msbl_580_4gawk_getline4.awk ${fgYellow}"
cat ./msbl_580_4gawk_getline4.awk 
echo "${txReset}"

gawk -f ./msbl_580_4gawk_getline4.awk < ialpha.txt 

echo "
${fgGreen}28) gawk -f ./msbl_580_4gawk_getline5.awk < ialpha.txt 
${fgCyan}cat ./msbl_580_4gawk_getline5.awk ${fgYellow}"
cat ./msbl_580_4gawk_getline5.awk 
echo "${txReset}"

gawk -f ./msbl_580_4gawk_getline5.awk < ialpha.txt 

echo "
${fgGreen}29) gawk -f ./msbl_582_4gawk_getline6.awk < ialpha.txt 
${fgCyan}
cat ./msbl_581_4gawk_toupper.sh ${fgYellow}"
cat ./msbl_581_4gawk_toupper.sh
echo "${fgCyan}
cat ./msbl_582_4gawk_getline6.awk ${fgYellow}"
cat ./msbl_582_4gawk_getline6.awk 
echo "${txReset}"

gawk -f ./msbl_582_4gawk_getline6.awk < ialpha.txt 

echo "
${fgGreen}29) gawk -f ./msbl_583_4gawk_getline7.awk 
${fgCyan}
cat ./msbl_583_4gawk_getline7.awk ${fgYellow}"
cat ./msbl_583_4gawk_getline7.awk 
echo "${txReset}"

#gawk -f ./msbl_583_4gawk_getline7.awk 

echo "${txReset}"
