Примеры по работе awl.
Источник: Марк Г.Собель "Linux. Администрирование и системное программирование."
Source: Mark G.Sobel "A Practical Guide to Linux Commands, Editors and Shell Programming." Second Edition.
Глава 12. Редактор awk.

File name:

<msbl>_<page>_<expl>.<awk/sh>
msbl - Mark Sobell
expl - explane

i* - input data
o* - output result
