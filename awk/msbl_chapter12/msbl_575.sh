#!/bin/bash

# Суммируем каждый столбец входного файла.
# Программа получает ввод и проверяет строку на отсутствие ошибок,
# сообщает о наличии строк не содержащих числовые записи, и
# не включает их в отчет
# next - пропускает команды для строки с ошибками.
# В конце программы gawk выводит для файла общую сумму чисел.

gawk ' BEGIN { ORS = "" }
NR == 1 {                                                               # Только для первой записи.
    nfields = NF                                                        # Присваиваем nfields количество полей в записи (NF).
}
{
    if ($0 ~ /[^0-9. \t]/) {                                            # Проверка каждой записи на содержание любых
    print "\nЗапись " NR  " пропущена:\n\t"                             # нечисловых символов, точек, пробелов или символов TAB.
    print $0 "\n"
    next                                                                # Пропуск неподходящих записей.
}
else
{
    for (count = 1; count <= nfields; count++)                          # Перебор всех полей подходящих записей.
    {
	printf "%10.2f", $count > "otally.txt"
	sum[count] += $count
	gtotal += $count
    }
    print "\n" > "otally.txt"
}
}
END     {                                                               # После обработки последней строки.
    for (count = 1; count <= nfields; count++)                          # Вывод суммарного значения.
    {
	print "   -------" > "otally.txt"
    }
    print "\n" > "otally.txt"
    for (count = 1; count <= nfields; count++)
    {
	printf "%10.2f", sum[count] > "otally.txt"
    }
    print "\n\n\tОбщая сумма " gtotal "\n" > "otally.txt"
} ' < inumbers.txt 
