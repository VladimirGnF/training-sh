#!/bin/bash

# Сценарий выводит частотный список использования слов в файле,
# Используем стиль С.

tr -cs 'a-zA-Z' '[\n*]' < $1 |
gawk ' {
    count[$1]++
}
END     {
    for (item in count) {
	if (count[item] > 4)
	{
	    printf "%-15s%3s\n", item, count[item]
	}
    }
}'
#|
#sort +lnr +0f -1
