#! /bin/bash

red='\e[1;31m%s\e[0m\n'
green='\e[1;32m%s\e[0m\n'
yellow='\e[1;33m%s\e[0m\n'
blue='\e[1;34m%s\e[0m\n'
magenta='\e[1;35m%s\e[0m\n'
cyan='\e[1;36m%s\e[0m\n'

# background color using ANSI escape

bgBlack=$(tput setab 0)   # black
bgRed=$(tput setab 1)     # red
bgGreen=$(tput setab 2)   # green
bgYellow=$(tput setab 3)  # yellow
bgBlue=$(tput setab 4)    # blue
bgMagenta=$(tput setab 5) # magenta
bgCyan=$(tput setab 6)    # cyan
bgWhite=$(tput setab 7)   # white

# foreground color using ANSI escape

fgBLack=$(tput setaf 0)   # black
fgRed=$(tput setaf 1)     # red
fgGreen=$(tput setaf 2)   # green
fgYellow=$(tput setaf 3)  # yellow
fgBlue=$(tput setaf 4)    # blue
fgMagenta=$(tput setaf 5) # magenta
fgCyan=$(tput setaf 6)    # cyan
fgWhite=$(tput setaf 7)   # white

# text editing options

txBold=$(tput bold)       # bold
txHalf=$(tput dim)        # half-bright
txUnderline=$(tput smul)  # underline
txEndUnder=$(tput rmul)   # exit underline
txReverse=$(tput rev)     # reverse
txStandout=$(tput smso)   # standout
txEndStand=$(tput rmso)   # exit standout
txReset=$(tput sgr0)      # reset attributes

echo "
${fgGreen}01. sed '/line/ p' lines.txt
${fgCyan}Строка выводит все строки файла, в которых содержится 
слово 'line'. Т.к. ключ -n отсутствует, то sed выводить все строки 
ввода. Поэтому sed выводит строки содержащие led дважды.
${txReset}"

sed '/line/ p' lines.txt

echo "
${fgGreen}02. sed -n '/line/ p' lines.txt
${fgCyan}Используется ключ -n, позтому sed выводит только выбранные строки.
${txReset}"

sed -n '/line/ p' lines.txt

echo "
${fgGreen}03. sed -n '3,6 p' lines.txt
Выводим строки с 3 по 6.
${txReset}"

sed -n '3,6 p' lines.txt

echo "
${fgGreen}04. sed '5 q' lines.txt
q - команда \"заверить\". Выводим первые 5 строк. 
Дублирование команды: head -5.
${txReset}"

sed '5 q' lines.txt

echo "
${fgGreen}05. sed -n -f ch13p591a.sh lines.txt 

${fgCyan}-f - читать программу из файла.
cat msbl_591a.sh 
${fgYellow}"
cat msbl_591a.sh 
echo "${txReset}"

sed -n -f msbl_591a.sh lines.txt

echo "
${fgGreen}06. Инструкция 'a' - добавить.

sed -f msbl_591b.sh 
${fgCyan}
cat msbl_591b.sh
${fgYellow}"
cat msbl_591b.sh 
echo "${txReset}"

sed -f msbl_591b.sh lines.txt

echo " 
${fgGreen}07. Инструкция 'i' - вставить.
sed -f msbl_592а.sh 
${fgCyan}Выбираем все строки, которые содержат This и вставляем 
разделитель строк и текст 'BEFORE' перед выбранной строкой.
${fgBlue=}cat msbl_592a.sh
${fgYellow}"
cat msbl_592а.sh 
echo "${txReset}"

sed -f msbl_592a.sh lines.txt

echo "
${fgGreen}08. Инструкция 'c' - изменить.
sed -f msbl_592b.sh 
${fgCyan}Инструкция 'c' не изменяет каждую строку внутри диапазона 
строк, а меняет блок, вставляя вместо него один экземпляр нового 
текста.
${fgBlue}cat msbl_592b.sh
${fgYellow}"
cat msbl_592b.sh
echo "${txReset}"

sed -f msbl_592b.sh lines.txt

echo "
${fgGreen}09. Инструкция 's' - подставить.
sed -f msbl_593a.sh ${fgCyan}
В данном примере sed выбирает все строки, т.к. диапазон 
не указан, в каждой строке программа заменяет первый экземпляр слова
line словом sentence. Флаг '-p' указывает выводить строки в которых 
произошла подстановка.
${fgBlue}cat msbl_593a.sh
${fgYellow}"
cat msbl_593a.sh 
echo "${txReset}"

sed -n -f msbl_593a.sh lines.txt

echo "
${fgGreen}10. Инструкция 'w' - записать.
sed -f msbl_593b.sh 
${fgCyan}Вывод в файл temp.
${fgBlue}cat msbl_593b.sh
${fgYellow}"
cat msbl_593b.sh 
echo "${txReset}"

sed -n -f msbl_593b.sh lines.txt

echo "
${fgGreen}11. Сценарий оболочки'.
./msbl_593c.sh file1 file2 file3
${fgCyan}В файлах file1, file2 и file3 провести следующие замены:
FILE -> file
PROCESS -> process.
${fgBlue}cat msbl_593c.sh
${fgYellow}"
cat msbl_593c.sh 
echo "${txReset}"

./msbl_593c.sh file1 file2 file3

echo "
${fgGreen}12. Инструкция 'w' - записать.
sed -f msbl_594a.sh 
${fgCyan}Копирует часть файла в другой файл.
${fgBlue}cat msbl_594a.sh
${fgYellow}"
cat msbl_594a.sh 
echo "${txReset}"

sed -n -f msbl_594a.sh lines.txt

echo "
${fgGreen}13. Инструкция 'w' - записать.
sed -f msbl_594b.sh
${fgCyan}Копирует часть файла в другой файл. Используем оператор HE (!)
${fgBlue}cat msbl_594b.sh 
${fgYellow}"
cat msbl_594b.sh 
echo "${txReset}"

sed -n -f msbl_594b.sh lines.txt

echo "
${fgGreen}14. Инструкция 'n' - следующий.
sed -f msbl_594c.sh
${fgCyan}При обработки данной строки sed приступает к обработки 
следующей строки.
${fgBlue}cat msbl_594c.sh 
${fgYellow}"
cat msbl_594c.sh 
echo "${txReset}"

sed -n -f msbl_594c.sh lines.txt

echo "
${fgGreen}15. Инструкция 'n' - следующий.
sed -f msbl_595a.sh
${fgYellow}
${fgCyan} С инструкцией \"Следующий\" используем текстовый адрес.
${fgBlue}cat msbl_595a.sh" 
cat msbl_595a.sh 
echo "${txReset}"

sed -n -f msbl_595a.sh lines.txt

echo "
${fgGreen}16. Инструкция 'N' - следующий.
sed -f 
${fgCyan} С инструкцией \"следующий\" с буквой N в верхнем регистре.
Инструкция добавляет следующую строку, к строке которая содержит  
слово the. В данном случае добавляется строка 7 к строке 6. Затем 
символ перевода строки меняем на пробел.
${fgBlue}cat msbl_595b.sh
${fgYellow}"
cat msbl_595b.sh 
echo "${txReset}"

sed -n -f msbl_595b.sh lines.txt

echo "
${fgGreen}17. Подстановка строковых значений.
sed -f msbl_596a.sh compound.in
${fgCyan}В строках 1-3 подстановка строкового значения words -> text.
В строках 2-4 подстановка строкового значения text -> TEXT.
${fgBlue}cat msbl_596a.sh 
${fgYellow}"
cat msbl_596a.sh 
echo "${txReset}"

sed -f msbl_596a.sh compound.in 

echo "
${fgGreen}18. Изменен порядок инструкций.
sed -f msbl_596b.sh compound.in
${fgCyan} В строках 2-4 подстановка строкового значения text -> TEXT.
${fgCyan} В строках 1-3 подстановка строкового значения words -> text.
${fgBlue}cat msbl_596b.sh 
${fgYellow}"
cat msbl_596b.sh 
echo "${txReset}"
sed -f msbl_596b.sh compound.in 

echo "
${fgGreen}19. Добавляем ко 2-ой строке ещё 2 строки.
sed -f msbl_596c.sh compound.in
${fgBlue}cat msbl_596c.sh
${fgYellow}"
cat msbl_596c.sh
echo "${txReset}"
sed -f msbl_596c.sh compound.in 

echo "
${fgGreen}20. Добавляем ко 2-ой строке ещё 2 строки.
${fgCyan} Удаляем 2 строки. Инструкция \"Добавить\" попрежнему выводит 
2 строки, добавленные в строке 2. Добавленные строки выводятся даже 
если в используется ключ -n.
${fgBlue}sed -f msbl_597a.sh compound.in"
"${fgBlue}cat msbl_597a.sh
${fgYellow}"
cat msbl_597a.sh
echo "${txReset}"

sed -f msbl_597a.sh compound.in 

echo "
${fgGreen}21. Используем регулярное выражение в качестве шаблона.
${fgCyan}Регулярному выражению ^. соответствует любому непустому
символу в начале строки. Знак & - превращается в значение строкого
выражения, которому соответствует регулярное выражение.
${fgBlue}sed 's/^./\t&/' lines.txt 
${txReset}"

sed 's/^./\t&/' lines.txt 

echo "
${fgGreen}22. Предыдущий пример ввиде сценария.
${fgBlue}./msbl_597b.sh lines.txt
${fgBlue}cat msbl_597b.sh"
cat msbl_597b.sh 
echo "${txReset}"

./msbl_597b.sh lines.txt 

echo "
${fgGreen}23. Удаляем замыкающие пробелы.
${fgCyan}Регулярному выражению (  *$ - два пробела за которыми следует *$)
соответствует одному или нескольким пробелам в конце строки.
${fgBlue}./msbl_598a.sh lines.txt
${fgBlue}cat msbl_598a.sh
${fgYellow}"
cat msbl_598a.sh 
echo "${txReset}"

./msbl_598a.sh lines.txt 

echo "
${fgGreen}24. Используем область хранения.
${fgBlue}sed -nf msbl_598b.sh lines.txt
${fgBlue}cat msbl_598b
${fgYellow}"
cat msbl_598b 
echo "${txReset}"

sed -nf msbl_598b lines.txt 

echo "
${fgGreen}25. Инструкция G.
${fgCyan}Инсрукция G добавляет в 'Область шаблона' разделитель строк
и содержимое области хранения.
${fgBlue}sed 'G' lines.txt 
${txReset}"

sed 'G' lines.txt 

echo "
${fgGreen}26. Меняем порядок строк в файле на обратный 
(точно так же как утилита tac).
${fgBlue}sed -f ./msbl_599.sh lines.txt
${fgBlue}cat msbl_599.sh
${fgYellow}"
cat msbl_599.sh 
echo "${txReset}"

sed -f ./msbl_599.sh lines.txt

