
# Инструкция 'Изменить' не изменяет каждую строку внутри указанного 
# диапазона, а меняет блок, вставляя вместо него один экзэмпляр
# нового текста.

# sed -f ./ch13p592b.sh lines.txt

2,4 c\
SED WILL INSERT\
THREE LINES IN PLACE\
OF THE SELECTED LINES	
